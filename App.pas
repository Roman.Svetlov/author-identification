﻿{$reference AuthorIdentification.dll}
{$reference System.Web.Extensions.dll}
uses system.IO, System.Collections.Generic, System.Web.Script.Serialization;

var
  a: TAId := new TAId();
  tx: string;
  an: string;
  fi: file;
  newtext: StringBuilder;
  file_console: text;

type
  Settings = record
    SortedN_gram: array of boolean := (true, true, true);
    TopOfN_gram: array of integer := (33, 100, 500);
    ParametersWeight: array of double := (0.2, 0.2, 0.1, 0.15, 0.35);
    Directoria: string := 'Books\';
  end;

var
  ThisSettings: Settings;

var
  e := double.Epsilon;


var
  a1: array of AuthorProfileToOutput;

procedure CreateAuthor(AName: string);
begin
  newtext := '';
  var files := Directory.GetFiles(ThisSettings.Directoria + AName, '*.txt', SearchOption.AllDirectories);
  foreach name: string in files do
  begin
    tx += ReadAllText(name + '');
    
    begin
      newtext := new StringBuilder(tx.Length);
      foreach Symbol: char in tx.ToCharArray do
        newtext.Append(Symbol);
    end;
  end;
  
  a.SetVanillaText(newtext);
  a.GetAuthor.SetAuthorName(AName);
  a.DeterminationOfAverageWordAndSentenceLength;
  a.TextPreparation;
  a.DeterminationOfN_gramDFD;
  a.AddOrReplace();
  
end;

procedure SAuthor(FName: string);
begin
  newtext := '';
  a.GetAuthor.SetAuthorName(an);
  
  tx := ReadAllText(FName {+ '.txt'});
  
  
  newtext := new StringBuilder(tx.Length);
  
  foreach Symbol: char in tx.ToCharArray do
    newtext.Append(Symbol);
  
  a.SetVanillaText(newtext);
  a.DeterminationOfAverageWordAndSentenceLength;
  a.TextPreparation;
  a.DeterminationOfN_gramDFD;
  
  a1 := a.AuthorIdentification;
  
  var temp: AuthorProfileToOutput := new AuthorProfileToOutput;
  for i: integer := 0 to a1.Length - 1 do
  begin
    for j: integer := 0 to a1.Length - 2 do
    begin
      if a1[j + 1].GetDistanceFromAuthorStandardToUserText < a1[j].GetDistanceFromAuthorStandardToUserText then
      begin
        temp := a1[j];
        a1[j] := a1[j + 1];
        a1[j + 1] := temp;
      end;
    end;
    
  end;
  
  for i: integer := 0 to a1.Length - 1 do
  begin
    println(' [' + (i + 1) + '] ' + a1[i].AuthorName + ' ' + a1[i].DistanceFromAuthorStandardToUserText);
    file_console := OpenAppend('cout.txt');
    println(file_console, ' [' + (i + 1) + '] ' + a1[i].AuthorName + ' ' + a1[i].DistanceFromAuthorStandardToUserText);
    file_console.Close;
  end;
end;

type
  stats = record
    NameAuthor: string;
    ChosenAuthor: string;
    Title: string;
    RangeToAuthor: double;
    Right: boolean;
    RangeOfMiss: integer;
    SizeOfThisText: uint64;
    SizeOfThisAuthor: uint64;
    SizeOfChosenAuthor: uint64;
  end;

var
  ThisStats: stats;



procedure LoadSettings();
var
  f: text;
  Lines: array of string;
  i: uint64 := 0;
  jss := new JavaScriptSerializer();
  Serialized := '';
  JsonFileName := 'Settings.txt';
begin
  if not FileExists(JsonFileName) then 
  begin
    Assign(f, JsonFileName);
    Rewrite(f);
    Close(f);
  end;
  Lines := ReadAllLines(JsonFileName);
  
  foreach Line: string in Lines do 
    ThisSettings := jss.Deserialize&<Settings>(Line);
  
  
end;

procedure SaveSettings();
var
  f: text;
  Lines: array of string;
  i: uint64 := 0;
  Replaced: Boolean := false;
  jss := new JavaScriptSerializer();
  Serialized := '';
  JsonFileName := 'Settings.txt';
begin
  
  Serialized := jss.Serialize(ThisSettings);
  Assign(f, JsonFileName);
  Rewrite(f);
  write(f, Serialized);
  Close(f);
  
  
end;

procedure SaveStats();
var
  f: text;
  Lines: array of string;
  i: uint64 := 0;
  jss := new JavaScriptSerializer();
  Serialized := '';
  JsonFileName := 'Stats.txt';
begin
  
  
  Serialized := jss.Serialize(ThisStats);
  
  Lines := ReadAllLines(JsonFileName);
  
  
  f := OpenAppend(JsonFileName);
  writeln(f, Serialized);
  f.Close;
  
  
  
  
  
end;

begin
  assign(file_console, 'cout.txt');
  rewrite(file_console);
  Close(file_console);
  var time := Milliseconds;
  var
  f: text;
  var JsonFileName := 'Stats.txt';
  
  Assign(f, JsonFileName);
  Rewrite(f);
  Close(f);
  
  LoadSettings();
  
  a.SetFileName('AuthorProfilesToAIL.json');
  
  a.SetTopOfN_gram(ThisSettings.TopOfN_gram);
  a.SetParametersWeight(ThisSettings.ParametersWeight);
  a.SetN_gramSorted(ThisSettings.SortedN_gram);
  a.LoadAuthorProfiles();
  
  var ans := Directory.GetDirectories(ThisSettings.Directoria);
  var i: integer;
  
  foreach line: string in ans do
  begin
    Delete(ans[i], Pos(ThisSettings.Directoria, ans[i]), ThisSettings.Directoria.Length);
    i += 1;
  end;
  
  
  i := 0;
  foreach line: string in ans do
  begin
    var files := Directory.GetFiles(ThisSettings.Directoria + line, '*.txt', SearchOption.AllDirectories);
    
    foreach name: string in files do
    begin
      var d := Milliseconds; 
      var tempi: integer := 0;
      var nameTN := name;
      Delete(nameTN, Pos(ThisSettings.Directoria, nameTN), (ThisSettings.Directoria).Length);
      
      newtext := new StringBuilder;
      tx := '';
      println('"' + nameTN + '": ');
      file_console := OpenAppend('cout.txt');
      println(file_console, '"' + nameTN + '": ');
      println(file_console, '');
      file_console.Close;
      println();
      SAuthor(name);
      
      ThisStats.NameAuthor := line;
      ThisStats.ChosenAuthor := a1[0].AuthorName;
      ThisStats.RangeToAuthor := a1[0].DistanceFromAuthorStandardToUserText;
      
      if (ThisStats.ChosenAuthor = ThisStats.NameAuthor) then 
      begin
        ThisStats.Right := true;
        
      end else ThisStats.Right := false;
      
      var temp: AuthorProfileToOutput := new AuthorProfileToOutput;
      for ii: integer := 0 to a1.Length - 1 do
      begin
        if line <> a1[ii].AuthorName then
        begin
          tempi += 1;
        end else break;
      end;
      
      foreach Profile: AuthorProfileToOutput in a1 do
        if line = Profile.AuthorName then 
          ThisStats.SizeOfThisAuthor := Profile.NumberOfChar;
      
      ThisStats.Title := name;
      ThisStats.RangeOfMiss := tempi;
      
      ThisStats.SizeOfThisText := a.GetAuthor.GetNumberOfChar;
      
      ThisStats.SizeOfChosenAuthor := a1[0].GetNumberOfChar;
      
      SaveStats();
      
      a.DataCleaning();
      
      println();
      println(nameTN + ' is completed in ' + ((Milliseconds - d) / 1000) + ' seconds and missed by ' + tempi + ';');
      
      file_console := OpenAppend('cout.txt');
      println(file_console, nameTN + ' is completed in ' + ((Milliseconds - d) / 1000) + ' seconds and missed by ' + tempi + ';');
      file_console.Close;
      
      for it: integer := 0 to System.Console.BufferWidth - 1 do
      begin
        write('-');
      end;
      
      file_console := OpenAppend('cout.txt');
      write(file_console, '----------------');
      println(file_console); 
      file_console.Close;
      println();
    end;
    
    println(line + ' is completed!');
    
    file_console := OpenAppend('cout.txt');
    println(file_console, line + ' is completed!');
    file_console.Close;
    
    
    for it: integer := 0 to System.Console.BufferWidth - 1 do
      begin
      write('-');
      end;
    println(); 
    file_console := OpenAppend('cout.txt');
    write(file_console, '----------------');
    println(file_console,'');
    file_console.Close;
    
    i += 1;
  end;
  
  println('Выполнено за ' + ((Milliseconds - time) / 1000) + 'секунд');
  
  file_console := OpenAppend('cout.txt');
  println(file_console, 'Выполнено за ' + ((Milliseconds - time) / 1000) + 'секунд');
  file_console.Close;
  
  readln();
end.



