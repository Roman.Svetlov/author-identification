﻿library AuthorIdentification;
{$reference System.Web.Extensions.dll}
uses System.Collections.Generic, System.Web.Script.Serialization;
//////////////////////////////////////////////////////////////
type
  AuthorProfile = class
  public 
    AuthorName: string; 
    NumberOfChar: uint64;
    NumberOfWord: uint64;
    NumberOfSentence: uint64;
    AverageWordLength: double;
    AverageSentenceLength: double;
    N_gramDFD: Dictionary<String, Double> := new Dictionary<String, double>;
    
    procedure SetAverageWordLength(input: Double);
    function GetAverageWordLength(): Double;
    procedure SetAverageSentenceLength(input: Double);
    function GetAverageSentenceLength(): Double;
    procedure SetN_gramDFD(input: Dictionary<String, Double>);
    function GetN_gramDFD(): Dictionary<String, Double>;
    procedure SetAuthorName(input: string);
    function GetAuthorName(): string;
    procedure SetNumberOfWord(input: uint64);
    function GetNumberOfWord(): uint64;
    procedure SetNumberOfChar(input: uint64);
    function GetNumberOfChar(): uint64;
    procedure SetNumberOfSentence(input: uint64);
    function GetNumberOfSentence(): uint64;
  end;

/////////////////////////////////////////////////////////////
type
  AuthorProfileToOutput = class
  public 
    AuthorName: string; 
    NumberOfChar: uint64;
    NumberOfWord: uint64;
    NumberOfSentence: uint64;
    AverageWordLength: double;
    AverageSentenceLength: double;
    DistanceFromAuthorStandardToUserText: double;
    
    function GetDistanceFromAuthorStandardToUserText(): Double := DistanceFromAuthorStandardToUserText;
    
    function GetAverageWordLength(): Double := AverageWordLength;
    
    function GetAverageSentenceLength(): Double := AverageSentenceLength;
    
    function GetAuthorName(): string := AuthorName;
    
    function GetNumberOfWord(): uint64 := NumberOfWord;
    
    function GetNumberOfChar(): uint64 := NumberOfChar;
    
    function GetNumberOfSentence(): uint64 := NumberOfSentence;
  
  internal 
    
    procedure SetDistanceFromAuthorStandardToUserText(input: Double);
    begin
      DistanceFromAuthorStandardToUserText := input;
    end;
    
    procedure SetAverageWordLength(input: Double);
    begin
      AverageWordLength := input;
    end;
    
    procedure SetAverageSentenceLength(input: Double);
    begin
      AverageSentenceLength := input;
    end;
    
    procedure SetAuthorName(input: string);
    begin
      AuthorName := input;
    end;
    
    procedure SetNumberOfWord(input: uint64);
    begin
      NumberOfWord := input;
    end;
    
    procedure SetNumberOfChar(input: uint64);
    begin
      NumberOfChar := input;
    end;
    
    procedure SetNumberOfSentence(input: uint64);
    begin
      NumberOfSentence := input;
    end;
  
  end;


///////////////////////////////////////////////////////////////

type
  TAId = class 
  
  internal 
    
    VanillaText: StringBuilder; 
    PreparedText: StringBuilder;
    ThisAuthor: AuthorProfile := new AuthorProfile();
    
    N_gramDFD: Dictionary<String, Double> := new Dictionary<String, Double>;
    N_gramList: Dictionary<String, Integer> := new Dictionary<String, integer>;
    
    N_gramSorted: array of boolean := new boolean[3]; //1,2,3-граммы
    TopOfN_gram: array  of Integer := new integer[3]; //1,2,3-граммы
    ParametersWeight: array  of double := new double[5]; //Длина слова, предложения, 1,2,3-граммы
    
    ABC := 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'.ToHashSet;
    CharOfSpace: char := ' ';
    jss := new JavaScriptSerializer();
    Serialized: string := '';
    AuthorProfiles: array of AuthorProfile;
    JsonFileName: string := 'AuthorProfilesToAIL.json';
  public 
    
    procedure SetN_gramSorted(Input: array of boolean);
    procedure SetTopOfN_gram(Input: array  of Integer);
    procedure SetParametersWeight(Input: array  of double);
    
    function GetN_gramSorted(): array  of boolean := Self.N_gramSorted;
    function GetTopOfN_gram(): array  of Integer := Self.TopOfN_gram; 
    function GetParametersWeight(): array  of double := Self.ParametersWeight; 
    
    procedure TextPreparation;
    procedure DeterminationOfAverageWordAndSentenceLength;
    procedure DeterminationOfN_gramDFD;
    procedure SetVanillaText(VanillaTextInput: StringBuilder);
    function GetAverageWordLength(): double;
    function GetAverageSentenceLength(): double;
    function GetN_gramDFD(): Dictionary<String, Double>;
    function GetAuthor(): AuthorProfile;
    procedure AddOrReplace();
    procedure Remove(AuthorName: string);
    function Get(AuthorName: string): AuthorProfile;
    procedure LoadAuthorProfiles();
    function AuthorIdentification: array of AuthorProfileToOutput;
    procedure SetFileName(input: string);
    function GetFileName: string;
    procedure DataCleaning();
  end;

procedure TAId.DataCleaning();
begin
  VanillaText := new StringBuilder; 
  PreparedText := new StringBuilder;
  Self.ThisAuthor := new AuthorProfile();
  
  N_gramDFD := new Dictionary<String, Double>;
  N_gramList := new Dictionary<String, integer>;
  
  jss := new JavaScriptSerializer();
  Serialized := '';
end;

procedure TAId.SetN_gramSorted(Input: array of boolean);
begin
  Self.N_gramSorted := Input;
end;

procedure TAId.SetTopOfN_gram(Input: array  of Integer);
begin
  Self.TopOfN_gram := Input;
end;

procedure TAId.SetParametersWeight(Input: array  of double);
begin
  Self.ParametersWeight := Input;
end;

procedure TAId.SetFileName(input: string);
begin
  Self.JsonFileName := input;
end;

function TAId.GetFileName: string := Self.JsonFileName;

function TAId.AuthorIdentification: array of AuthorProfileToOutput;

var
  tempUnigram: array of double := new double[AuthorProfiles.Length];
  tempBigram:  array of double := new double[AuthorProfiles.Length];
  tempTrigram:  array of double := new double[AuthorProfiles.Length];
  tempSentenceLength:  array of double := new double[AuthorProfiles.Length];
  tempWordLength:  array of double := new double[AuthorProfiles.Length];
  temp:  array of double := new double[AuthorProfiles.Length];
  Max: array of double := new double[5];
  Min: array of double := new double[5];
  TempAuthorProfileToOutput: array of AuthorProfileToOutput;
  i: uint64 := 0;
  tempSort: array of double := new double[ABC.Count * ABC.Count * ABC.Count];
begin
  
  for it: integer := 0 to Min.Length - 1 do
    Min[i] := Double.MaxValue;
  
  for it: integer := 0 to Min.Length - 1 do
    Max[i] := Double.MinValue;
  
  if (AuthorProfiles = nil) then LoadAuthorProfiles();
  ///////////////////////////////
  if (Self.N_gramSorted[2] = true) then 
  begin
    foreach x: char in ABC do
      foreach y: char in ABC do
        foreach z: char in ABC do
        begin
          tempSort[i] := Self.ThisAuthor.N_gramDFD.Item[concat(x, y, z)];
          i += 1;
        end;
    i := 0;
    
    tempSort := tempSort.SortedDescending.ToArray;
    SetLength(tempSort, Self.TopOfN_gram[2]);
    
    foreach x: char in ABC do
      foreach y: char in ABC do
        foreach z: char in ABC do
        begin
          if Self.ThisAuthor.N_gramDFD.Item[concat(x, y, z)] < tempSort[tempSort.Length - 1] then
            Self.ThisAuthor.N_gramDFD.Remove(concat(x, y, z));
          i += 1;
        end;
    
    i := 0;
  end;
  
  /////////////////////////////////
  if (Self.N_gramSorted[1] = true) then 
  begin
    tempSort := new double[ABC.Count * ABC.Count];
    foreach x: char in ABC do
      foreach y: char in ABC do
      begin
        tempSort[i] := Self.ThisAuthor.N_gramDFD.Item[concat(x, y)];
        i += 1;
      end;
    i := 0;
    tempSort := tempSort.SortedDescending.ToArray;
    SetLength(tempSort, Self.TopOfN_gram[1]);
    
    foreach x: char in ABC do
      foreach y: char in ABC do
      begin
        if Self.ThisAuthor.N_gramDFD.Item[concat(x, y)] < tempSort[tempSort.Length - 1] then
          Self.ThisAuthor.N_gramDFD.Remove(concat(x, y));
        i += 1;
      end;
    i := 0;
  end;
  
  ///////////////////////////////////////////////////////////////
  if (Self.N_gramSorted[0] = true) then 
  begin
    tempSort := new double[ABC.Count];
    foreach x: char in ABC do
    begin
      tempSort[i] := Self.ThisAuthor.N_gramDFD.Item[x];
      i += 1;
    end;
    i := 0;
    tempSort := tempSort.SortedDescending.ToArray;
    SetLength(tempSort, Self.TopOfN_gram[0]);
    
    foreach x: char in ABC do
    begin
      if Self.ThisAuthor.N_gramDFD.Item[x] < tempSort[tempSort.Length - 1] then
        Self.ThisAuthor.N_gramDFD.Remove(x);
      i += 1;
    end;
    i := 0;
  end;
  
  i := 0;
  
  //////////////////////////////////////////////////////////
  foreach Profile: AuthorProfile in AuthorProfiles do
  begin
    tempUnigram[i] := 0;
    tempBigram[i] := 0;
    tempTrigram[i] := 0;
    temp[i] := 0;
    foreach x: char in ABC do
    begin
      if (Profile.N_gramDFD.ContainsKey(x) and Self.ThisAuthor.N_gramDFD.ContainsKey(x))  then
        tempUnigram[i] += (ABS(Profile.GetN_gramDFD.Item[x] - Self.ThisAuthor.GetN_gramDFD.Item[x]));
      foreach y: char in ABC do
      begin
        if (Profile.N_gramDFD.ContainsKey(concat(x, y)) and Self.ThisAuthor.N_gramDFD.ContainsKey(concat(x, y))) then
          tempBigram[i] += (ABS(Profile.GetN_gramDFD.Item[Concat(x, y)] - Self.ThisAuthor.GetN_gramDFD.Item[Concat(x, y)]));
        foreach z: char in ABC do
        begin
          if (Profile.N_gramDFD.ContainsKey(concat(x, y, z)) and Self.ThisAuthor.N_gramDFD.ContainsKey(concat(x, y, z))) then
            tempTrigram[i] += (ABS(Profile.GetN_gramDFD.Item[Concat(x, y, z)] - Self.ThisAuthor.GetN_gramDFD.Item[Concat(x, y, z)]));
        end;
      end;
    end;
    
    
    tempSentenceLength[i] := ABS(Self.ThisAuthor.GetAverageSentenceLength - Profile.AverageSentenceLength);
    tempWordLength[i] := ABS(Self.ThisAuthor.GetAverageWordLength - Profile.AverageWordLength);
    
    if (tempWordLength[i] < Min[0]) then Min[0] := tempWordLength[i];
    if (tempSentenceLength[i] < Min[1]) then Min[1] := tempSentenceLength[i];
    if (tempUnigram[i] < Min[2]) then Min[2] := tempUnigram[i];
    if (tempBigram[i] < Min[3]) then Min[3] := tempBigram[i];
    if (tempTrigram[i] < Min[4]) then Min[4] := tempTrigram[i];
    
    if (tempWordLength[i] > Max[0]) then Max[0] := tempWordLength[i];
    if (tempSentenceLength[i] > Max[1]) then Max[1] := tempSentenceLength[i];
    if (tempUnigram[i] > Max[2]) then Max[2] := tempUnigram[i];
    if (tempBigram[i] > Max[3]) then Max[3] := tempBigram[i];
    if (tempTrigram[i] > Max[4]) then Max[4] := tempTrigram[i];
    
    SetLength(TempAuthorProfileToOutput, i + 1);
    TempAuthorProfileToOutput[i] := new AuthorProfileToOutput;
    
    TempAuthorProfileToOutput[i].SetAuthorName(Profile.GetAuthorName);
    TempAuthorProfileToOutput[i].SetDistanceFromAuthorStandardToUserText(temp[i]);
    TempAuthorProfileToOutput[i].SetAverageSentenceLength(Profile.GetAverageSentenceLength);
    TempAuthorProfileToOutput[i].SetAverageWordLength(Profile.GetAverageWordLength);
    TempAuthorProfileToOutput[i].SetNumberOfChar(Profile.GetNumberOfChar);
    TempAuthorProfileToOutput[i].SetNumberOfSentence(Profile.GetNumberOfSentence);
    TempAuthorProfileToOutput[i].SetNumberOfWord(Profile.GetNumberOfWord);
    i += 1;
    
  end;
  
  i := 0;
  
  foreach Profile: AuthorProfile in AuthorProfiles do
  begin
    var tempRange: double := 0; 
    
    if tempWordLength[i] <> 0 then
      tempRange += ABS(ABS(tempWordLength[i] / ABS(Max[0])) * ABS(Self.ParametersWeight[0]));
    
    if tempSentenceLength[i] <> 0 then
      tempRange += ABS(ABS(tempSentenceLength[i] / ABS(Max[1])) * ABS(Self.ParametersWeight[1]));
    
    if tempUnigram[i] <> 0 then
      tempRange += ABS(ABS(tempUnigram[i] / ABS(Max[2])) * ABS(Self.ParametersWeight[2]));
    
    if tempBigram[i] <> 0 then
      tempRange += ABS(ABS(tempBigram[i] / ABS(Max[3])) * ABS(Self.ParametersWeight[3]));
    
    if tempTrigram[i] <> 0 then
      tempRange += ABS(ABS(tempTrigram[i] / ABS(Max[4])) * ABS(Self.ParametersWeight[4]));
    //без нормализации:
    {if tempWordLength[i] <> 0 then
      tempRange += ABS(ABS(tempWordLength[i]) * ABS(Self.ParametersWeight[0]));
    
    if tempSentenceLength[i] <> 0 then
      tempRange += ABS(ABS(tempSentenceLength[i]) * ABS(Self.ParametersWeight[1]));
    
    if tempUnigram[i] <> 0 then
      tempRange += ABS(ABS(tempUnigram[i]) * ABS(Self.ParametersWeight[2]));
    
    if tempBigram[i] <> 0 then
      tempRange += ABS(ABS(tempBigram[i]) * ABS(Self.ParametersWeight[3]));
    
    if tempTrigram[i] <> 0 then
      tempRange += ABS(ABS(tempTrigram[i]) * ABS(Self.ParametersWeight[4]));}
    
    TempAuthorProfileToOutput[i].SetDistanceFromAuthorStandardToUserText(tempRange);
    
    i += 1;
  end;
  
  result := TempAuthorProfileToOutput;
end;

procedure TAId.LoadAuthorProfiles();
var
  i: uint64 := 0;
  f: text;
begin
  if not FileExists(Self.JsonFileName) then 
  begin
    assign(f, Self.JsonFileName); 
    Rewrite(f);
    Close(f);
  end;
  
  var
  Lines: array of string;
  begin
    
    Lines := ReadAllLines(Self.JsonFileName);
    
    foreach Line: string in Lines do 
    begin
      SetLength(AuthorProfiles, i + 1);
      AuthorProfiles[i] := new AuthorProfile;
      AuthorProfiles[i] := jss.Deserialize&<AuthorProfile>(Line);
      i += 1;
    end;
    
  end;
  
end;

procedure TAId.AddOrReplace();
var
  f: text;
  Lines: array of string;
  i: uint64 := 0;
  Replaced: Boolean := false;
begin
  
  Serialized := '';
  Serialized := jss.Serialize(Self.ThisAuthor);
  
  Lines := ReadAllLines(Self.JsonFileName);
  
  foreach Line: string in Lines do 
  begin
    
    if Line.Contains(Self.ThisAuthor.GetAuthorName) then 
    begin
      Lines[i] := Serialized;
      Replaced := true;
    end;
    
    i += 1;
  end;
  
  if Replaced then WriteAllLines(Self.JsonFileName, Lines);
  
  if not Replaced then
  begin
    f := OpenAppend(Self.JsonFileName);
    writeln(f, Serialized);
    f.Close;
  end;
  
end;

procedure TAId.Remove(AuthorName: string);
var
  f: text;
  Lines: array of string;
  i: uint64 := 0;
  Remove: Boolean := false;
begin
  Lines := ReadAllLines(Self.JsonFileName);
  
  foreach Line: string in Lines do 
  begin
    if Line.Contains(AuthorName) then 
      if i < Lines.Length - 1 - 1 then
      begin
        for  j: integer := i to Lines.Length - 2 do
          Lines[i] := Lines[i + 1];
        Remove := true;
      end else SetLength(Lines, Lines.Length - 1);
    i += 1;
  end;
  
  if Remove then
  begin
    WriteAllLines(Self.JsonFileName, Lines);
  end;
end;

function TAId.Get(AuthorName: string): AuthorProfile;
var
  Lines: array of string;
begin
  Lines := ReadAllLines(Self.JsonFileName);
  foreach Line: string in Lines do 
  begin
    if Line.Contains(AuthorName) then 
      result := jss.Deserialize&<AuthorProfile>(Line);
  end;
end;

procedure TAId.TextPreparation;
begin
  PreparedText := new StringBuilder(VanillaText.Length);
  foreach Symbol: char in VanillaText.ToString do
  begin
    if (ABC.Contains(Symbol.ToLower) = true) then
      PreparedText.Append(Symbol.ToLower);
  end;
end;

procedure TAId.DeterminationOfAverageWordAndSentenceLength;
var
  EndOfSentenceSign := '.?!…'.ToHashSet;
  ThisIsWord: boolean := false;
  ThisIsSentence: boolean := false;

begin
  foreach Symbol: char in (VanillaText.Append(CharOfSpace)).ToString do
  begin
    if (ABC.Contains(Symbol.ToLower) = false) then
    begin
      if (ThisIsWord = true) then 
        Self.ThisAuthor.SetNumberOfWord(Self.ThisAuthor.GetNumberOfWord() + 1);
      
      ThisIsWord := false; 
    end
    else
    begin
      ThisIsWord := true;
      Self.ThisAuthor.SetNumberOfChar(Self.ThisAuthor.GetNumberOfChar + 1);
    end;
    
    if (EndOfSentenceSign.Contains(Symbol.ToLower) = false) then
    begin
      if (ThisIsSentence = true) then 
        Self.ThisAuthor.SetNumberOfSentence(Self.ThisAuthor.GetNumberOfSentence() + 1);
      
      ThisIsSentence := false; 
    end
    else
    begin
      ThisIsSentence := true;
    end;
    
  end;
  Self.ThisAuthor.SetAverageSentenceLength(Self.ThisAuthor.GetNumberOfWord / Self.ThisAuthor.GetNumberOfSentence);// := ThisAuthor.GetNumberOfWords / ThisAuthor.GetNumberOfSentence;
  Self.ThisAuthor.SetAverageWordLength(Self.ThisAuthor.GetNumberOfChar / Self.ThisAuthor.GetNumberOfWord);// := ThisAuthor.GetNumberOfChar / ThisAuthor.GetNumberOfWords;
end;

procedure TAId.DeterminationOfN_gramDFD;
begin
  
  foreach x: char in ABC do
  begin
    if (N_gramList.ContainsKey(x) = false) then 
      N_gramList.Add(x, 0);
    
    if (N_gramDFD.ContainsKey(x) = false) then 
      N_gramDFD.Add(x, 0);
    
    foreach y: char in ABC do
    begin
      if (N_gramList.ContainsKey(Concat(x, y)) = false) then 
        N_gramList.Add(Concat(x, y), 0);
      
      if (N_gramDFD.ContainsKey(Concat(x, y)) = false) then 
        N_gramDFD.Add(Concat(x, y), 0);
      
      foreach z: char in ABC do
      begin
        if (N_gramList.ContainsKey(Concat(x, y, z)) = false) then 
          N_gramList.Add(Concat(x, y, z), 0);
        
        if (N_gramDFD.ContainsKey(Concat(x, y, z)) = false) then 
          N_gramDFD.Add(Concat(x, y, z), 0);
      end;
    end;
  end;
  
  for i: uint64 := 0 to PreparedText.Length - 3 do
  begin
    if N_gramList.ContainsKey(Concat(PreparedText[i], PreparedText[i + 1], PreparedText[i + 2])) then 
      N_gramList.Item[Concat(PreparedText[i], PreparedText[i + 1], PreparedText[i + 2])] += 1; 
  end;
  
  for i: uint64 := 0 to PreparedText.Length - 2 do
  begin
    if N_gramList.ContainsKey(Concat(PreparedText[i], PreparedText[i + 1])) then 
      N_gramList.Item[Concat(PreparedText[i], PreparedText[i + 1])] += 1; 
  end;
  
  for i: uint64 := 0 to PreparedText.Length - 1 do
  begin
    if N_gramList.ContainsKey(PreparedText[i]) then 
      N_gramList.Item[PreparedText[i]] += 1; 
  end;
  
  
  foreach x: char in ABC do
  begin
    N_gramDFD.Item[x] := N_gramList[x] / PreparedText.Length;
    foreach y: char in ABC do
    begin
      N_gramDFD.Item[Concat(x, y)] := N_gramList[Concat(x, y)] / PreparedText.Length;
      foreach z: char in ABC do
      begin
        N_gramDFD.Item[Concat(x, y, z)] := N_gramList[Concat(x, y, z)] / PreparedText.Length;
      end;
    end;
  end;
  Self.ThisAuthor.SetN_gramDFD(N_gramDFD);
  
end;

procedure TAId.SetVanillaText(VanillaTextInput: StringBuilder);
var
  TempString: string;
begin
  TempString := Trim(VanillaTextInput.ToString); 
  VanillaText := new StringBuilder(VanillaTextInput.Length);
  foreach Symbol: char in TempString.ToCharArray do
    VanillaText.Append(Symbol);
end;

function TAId.GetAverageWordLength(): double := Self.ThisAuthor.GetAverageWordLength;

function TAId.GetAverageSentenceLength(): double := Self.ThisAuthor.GetAverageSentenceLength;

function TAId.GetN_gramDFD(): Dictionary<String, Double> := Self.ThisAuthor.getN_gramDFD;

function TAId.GetAuthor(): AuthorProfile;
begin
  result := Self.ThisAuthor;
end;
/////////////////////////////////////////////////////////////////////
procedure AuthorProfile.SetAverageWordLength(input: Double);
begin
  AverageWordLength := input;
end;

function AuthorProfile.GetAverageWordLength(): Double;
begin
  result := AverageWordLength;
end;

procedure AuthorProfile.SetAverageSentenceLength(input: Double);
begin
  AverageSentenceLength := input;
end;

function AuthorProfile.GetAverageSentenceLength(): Double;
begin
  result := AverageSentenceLength;
end;

procedure AuthorProfile.SetN_gramDFD(input: Dictionary<String, Double>);
begin
  N_gramDFD := input;
end;

function AuthorProfile.GetN_gramDFD(): Dictionary<String, Double>;
begin
  result := N_gramDFD;
end;

procedure AuthorProfile.SetAuthorName(input: string);
begin
  AuthorName := input;
end;

function AuthorProfile.GetAuthorName(): string := AuthorName;

procedure AuthorProfile.SetNumberOfWord(input: uint64);
begin
  NumberOfWord := input;
end;

function AuthorProfile.GetNumberOfWord(): uint64;
begin
  result := NumberOfWord;
end;

procedure AuthorProfile.SetNumberOfChar(input: uint64);
begin
  NumberOfChar := input;
end;

function AuthorProfile.GetNumberOfChar(): uint64;
begin
  result := NumberOfChar;
end;

procedure AuthorProfile.SetNumberOfSentence(input: uint64);
begin
  NumberOfSentence := input;
end;

function AuthorProfile.GetNumberOfSentence(): uint64;
begin
  result := NumberOfSentence;
end;
/////////////////////////////////////////////////////////////////////


end.