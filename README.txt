Использование простейшей программной оболочки для библиотеки:

1. Установить настройки в файле Settings.txt в папке с Author
Identification.exe (если файла нет — создать его):

{"SortedN_gram":[true,true,true],"TopOfN_gram":[33,16,300],"Pa
rametersWeight":[0,0,0,0,1],"Directoria":"E:\\Загрузки\\Книги\\"}

Параметры:

"SortedN_gram":[true,true,true] — использовать ли топ
значений уни, би и триграмм соответственно. (true либо false)

"TopOfN_gram":[33,16,300] — размер топа уни, би и триграмм
соответственно. (максимальные значения это 33, 1089, 35937).
Только целые числа.

"ParametersWeight":[0,0,0,0,1] — вес (вклад) каждого параметра 
в итоговый результат оценки. Средняя длина слова, предложения, 
частота униграммы, биграммы и триграммы соответственно. 
Сумма чисел <=1. Значения от 0 до 1. Дробная
часть отделяется точкой.

"Directoria":"E:\\Загрузки\\Книги\\" это путь до папки, в
которой лежат папки с авторами. Слэши должны быть
двойными. Путь полным. Папки авторов должны иметь имя
автора (или любой другой тип идентификатора, с помощью
которого Вы сможете определить автора).

По умолчанию программа берёт анализируемый текст из папки 'Books',
которая лежит там же, где и сама программа (параметр "Directoria"),
после чего сравнивает его с одним из 30 авторских профилей.
---------------------------------------------------------------------
2. Запустить Author Identification.exe
---------------------------------------------------------------------
3. Вывод осуществляется параллельно в консоль с работающей программой
и в файл 'cout.txt' в папке с программой. 
ВНИМАНИЕ: выходной файл перезаписывается при каждом запуске программы!
---------------------------------------------------------------------
4. В файле 'Stats.txt' лежит какая-то статистика, сами разберётесь. 
---------------------------------------------------------------------
5. Запуск файла 'New Author Profiles.exe' создаст новый файл авторских 
профилей 'AuthorProfilesToAIL_NEW.json', взяв их из 
("Directoria":"Books\\" это путь до папки, в
которой лежат папки с авторами.)
Каждый авторский профиль будет иметь имя одной из папок внутри этой 
папки, и содержать данные, собранные из текста внутри папки автора. 
Для использования нового файла авторских профилей, переименуйте его в 
'AuthorProfilesToAIL.json'. Старую версию рекомендуется сохранить 
отдельно, во избежание. 
---------------------------------------------------------------------